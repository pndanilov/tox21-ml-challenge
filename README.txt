This dataset was obtained from the Tox21 Competition. It indicates whether particular compounds are 
toxic to the mitochondria or not.

To Run -

1. Navigate to "Tox21 ML Challenge\SR-MMP_Dataset\code"

2A. Run "Classification_Fingerprints_SVM.ipynb" in a jupyter notebook (the method I used)
 B. Run "Classification_Fingerprints_SVM.py" in a terminal/command window/ or some python shell
 C. (A "Classification_Fingerprints_SVM.txt" is also included for reference)

***The following python packages must be installed -
-numpy
-csv
-sklearn
-pandas
-rdkit

**Please, do not change the directory structure

3. Output files will be placed in "Tox21_ML_Challenge\SR-MMP_Dataset\run_1". See further in README for
explanation of files.



The goal was to develop a machine learning algorithm to predict chemical toxicity. In short, 
it was a binary classification problem.

The original dataset consisted of about 7,300 compounds of which only about 15% were active (toxic).
Though I could not provide it here due private information, I wrote a pre-processor program that
curated and cleaned the original dataset and split the data into training and test sets (75% - Train,
25% - Test). A big component of process involved removing chemical compounds that could not be used 
with the machine learning algorithms I will describe later. Each respective set had a -

"Clean_Row" field - which was the new index of the dataset row after invalid compounds were removed
"Orig_Row" field - the old index of the dataset before invalid compounds were removed
"Name/ID" field - the original identifier of the compound obtained from the Tox21 dataset
"Canonical_SMILES" field - a chemical formula (string) that describes a compounds structure
"Activity" field - 1 -> toxic, 0 -> non-toxic
"Set" - weather it's the test or training set (or the combined set)

My input to the program was the SMILES string and the label would be the activity (1/0). But, in this
algorithm, I wasn't working directly with SMILES strings. Instead, I obtained a chemical fingerprint 
from each SMILES string and used that as my input. A chemical fingerprint, also known as a descriptor,
is a 2048 bit vector that describes chemical properties of a compound such as charge, bond length, 
polarity, aromatic rings, etc. To create these fingerprints I used a well known chemistry toolkit
called RDKit.

Finally, I trained a support vector machine (SVM) with my training set, and made predictions for my test set.
For this project, I went with SciKit Learn's implementation of an SVM due to its concise code and ease
of adjusting hyperparameters.

My program creates three output files. The first, named 'results.csv' just shows the 
predicted activity for each compound in the test set. The next file 'train.csv', does the same,
but shows predictions for the training set. Basically, I made predictions on the original training
data, as a crude test for overfitting. Finally, 'summary.csv' reports metrics for how well my svm 
classifier performed on the test set. The four main metrics are sensitivity, specificity, precision, 
and AUC-ROC.

Lastly, I want to make it clear that this is not an optimized model and it shouldn't be looked at as
one. The code I wrote here is a proof of concept to show my abilities in creating a machine learning
model from start to finish. Not only did this entail using the core SVM algorithm itself, but
it also included pre-processing the data, and evaluating the results with meaningful
metrics. Yes, by optimizing the hyperparameters, the core SVM model could be improved to an
extent. However, as I've worked on many other machine learning projects for predicting 
chemical toxicity, I've learned many other techniques that outperform SVMs with chemical fingerprints 
altogether. Better suited ml algorithms for chemical classification along with fine tuned hyper 
parameters, and other sources of input that represents chemical structures, I've achieved much 
better results. In fact, I was able to surpass AUC-ROC, sensitivity, and specificity scores of 0.95 
each. Unfortunately again, I cannot provide this code here because it has private information. 
Nevertheless, I will be very happy to discuss more powerful techniques over an interview.
