import numpy as np                #Import all the necessary data manipulation and ML libraries
import csv as csv
import ast
import pandas as pd
from rdkit import Chem
from rdkit.Chem.Fingerprints import FingerprintMols
from sklearn import svm

#Store all of the paths and arguments
f = open(r'..\run_1\param.txt', 'r')

for i in range(11):
    f.readline()

f.readline()
train_set_path = f.readline().rstrip('\n')

f.readline()
f.readline()
test_set_path = f.readline().rstrip('\n')

f.readline()
f.readline()
results_path = f.readline().rstrip('\n')

f.readline()
f.readline()
train_results_path = f.readline().rstrip('\n')

f.readline()
f.readline()
summary_path = f.readline().rstrip('\n')

train_set = pd.read_csv(train_set_path)
test_set = pd.read_csv(test_set_path)

train_set.head(5)

#Convert all data to numpy arrays
train_features = np.array(train_set.drop(columns=['Activity']))
test_features = np.array(test_set.drop(columns=['Activity']))
train_labels = np.array(train_set['Activity'])
test_labels = np.array(test_set['Activity'])

#Calculate Statistics
train_compounds_count = len(train_labels)
test_compounds_count = len(test_labels)
total_compounds_count = train_compounds_count + test_compounds_count

train_class_1_count = 0
train_class_0_count = 0
test_class_1_count = 0
test_class_0_count = 0

for label in train_labels:
    if label == 1:
        train_class_1_count += 1
    else:
        train_class_0_count += 1

for label in test_labels:
    if label == 1:
        test_class_1_count += 1
    else:
        test_class_0_count += 1

#Convert Smiles to fingerprints
train_molecules = [Chem.MolFromSmiles(sample[3]) for sample in train_features]
train_fingerprints = np.array([FingerprintMols.FingerprintMol(mol) for mol in train_molecules])

test_molecules = [Chem.MolFromSmiles(sample[3]) for sample in test_features]
test_fingerprints = np.array([FingerprintMols.FingerprintMol(mol) for mol in test_molecules])

#Convert fingerprints to list of ints for RF training
train_fingerprints_ints = np.zeros((train_compounds_count, 2048), dtype=int)
test_fingerprints_ints = np.zeros((test_compounds_count, 2048), dtype=int)

for i, sample in enumerate(train_fingerprints):
    for j in range(len(sample)):
        train_fingerprints_ints[i][j] = sample[j]

for i, sample in enumerate(test_fingerprints):
    for j in range(len(sample)):
        test_fingerprints_ints[i][j] = sample[j]

# Setting up the SVM classifier and training it, then make test set predictions
clf = svm.SVC(class_weight='balanced', degree=3, kernel='poly')
clf.fit(train_fingerprints_ints, train_labels)

predictions = clf.predict(test_fingerprints_ints)

conf_matrix_test = np.empty([len(predictions)], dtype=object)

# Reporting the Accuracy, Precision, Sensitivity, and Specificity
#from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score

def get_metrics(predictions, labels, conf_matrix, the_set):
    true_positives, true_negatives, false_positives, false_negatives, predicted_positive = 0, 0, 0, 0, 0

    for i in range(len(predictions)):
        if predictions[i] == 1:
            predicted_positive = predicted_positive + 1

        if labels[i] == 1:
            if predictions[i] == 1:
                conf_matrix[i] = 'TP'
                true_positives = true_positives + 1
            else:
                conf_matrix[i] = 'FN'
                false_negatives = false_negatives + 1
        else:
            if predictions[i] == 1:
                conf_matrix[i] = 'FP'
                false_positives = false_positives + 1
            else:
                conf_matrix[i] = 'TN'
                true_negatives = true_negatives + 1

    if true_positives != 0:
        precision = true_positives/predicted_positive
    else:
        precision = 0
    
    #balanced_accuracy = balanced_accuracy_score(labels, predictions)
    
    total_positives = true_positives + false_negatives
    total_negatives = true_negatives + false_positives
    
    sensitivity = true_positives / (true_positives + false_negatives)
    specificity = true_negatives / (true_negatives + false_positives)
    
    accuracy = accuracy_score(labels, predictions)
    
    roc_score = roc_auc_score(labels, predictions)
    
    if false_negatives != 0:
        tp_fn = true_positives/false_negatives
    else:
        tp_fn = true_positives
    
    print("--------------------------- Metrics ------------------------------\n")
    
    #print("Balanced accuracy: " + str(round(balanced_accuracy, 2)) + "\n")
    print("Sensitivity (True Positive Rate): " + str(round(sensitivity, 2)) + "\n")
    print("Specificity (True Negative Rate): " + str(round(specificity, 2)) + "\n")
    print("ROC score: " + str(round(roc_score, 2)) + "\n")
    print("TP/FN: " + str(round(tp_fn, 2)) + "\n")
    print("Accuracy: " + str(round(accuracy, 2)) + "\n")
    print("Precision (Percentage of Class 1 predictions that were correct): " + str(round(precision, 2)))
    
    print("\n------------------------------------------------------------------")
    
    print("\nTotal Positives: " + str(total_positives) + "   Total Negatives: " + str(total_negatives) + "\n")
    print("True Positives: " + str(true_positives) + "   False Positives: " + str(false_positives) + "\n")
    print("False Negatives: " + str(false_negatives) + "   True Negatives: " + str(true_negatives))
    
    if the_set == 'test':
        mode = 'w'
    elif the_set == 'train':
        mode = 'a'
    
    with open(summary_path, mode, newline='') as summary_file:
        summary_Csv = csv.writer(summary_file)

        if the_set == 'test':
            summary_Csv.writerow(["Total Test Compounds", test_compounds_count])
        elif the_set == 'train':
            summary_Csv.writerow([])
            summary_Csv.writerow([])
            summary_Csv.writerow(["Total Training Compounds", train_compounds_count])
        
        summary_Csv.writerow(["Positives", true_positives + false_negatives])
        summary_Csv.writerow(["Negatives", true_negatives + false_positives])
        
        summary_Csv.writerow([])
        
        summary_Csv.writerow(["ROC_Score", round(roc_score, 2)])
        summary_Csv.writerow(["Sensitivity (True Positive Rate)", round(sensitivity, 2)])
        summary_Csv.writerow(["Specificity (True Negative Rate)", round(specificity, 2)])
        summary_Csv.writerow(["Accuracy", round(accuracy, 2)])
        summary_Csv.writerow(["Precision", round(precision, 2)])
        summary_Csv.writerow(["TP/FN", round(tp_fn, 2)])

        summary_Csv.writerow([])

        summary_Csv.writerow(["Total Positives", total_positives, "",  "Total Negatives", total_negatives])
        summary_Csv.writerow(["True Positives", true_positives, "", "False Postives", false_positives])
        summary_Csv.writerow(["False Negatives", false_negatives, "", "True Negatives", true_negatives])

get_metrics(predictions, test_labels, conf_matrix_test, 'test')

# Save results to csv
with open(results_path, 'w', newline='') as outfile:
    forest_results_Csv = csv.writer(outfile)
    forest_results_Csv.writerow(["Row_Num", "Orig_Num", "Name/ID", 
                                 "Canonical_SMILES", "Actual_Class", 
                                 "Predicted_Class", "Confusion_Matrix"])
    
    for i, sample in enumerate(test_features, 0):
        forest_results_Csv.writerow([sample[0], sample[1], sample[2], 
                                     sample[3], test_labels[i], 
                                     predictions[i], conf_matrix_test[i]])

#Run Train data through model to test for overfitting
train_predictions = clf.predict(train_fingerprints_ints)
conf_matrix_train = np.empty([len(train_predictions)], dtype=object)

get_metrics(train_predictions, train_labels, conf_matrix_train, 'train')

# Save train results to csv
with open(train_results_path, 'w', newline='') as outfile:
    forest_results_Csv = csv.writer(outfile)
    forest_results_Csv.writerow(["Row_Num", "Orig_Num", "Name/ID", 
                                 "Canonical_SMILES", "Actual_Class", 
                                 "Predicted_Class", "Confusion_Matrix"])
    
    
    for i, sample in enumerate(train_features, 0):
        forest_results_Csv.writerow([sample[0], sample[1], sample[2], 
                                     sample[3], train_labels[i], 
                                     train_predictions[i], conf_matrix_train[i]])